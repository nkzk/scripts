#!/bin/bash

# Sets the different variables to be equal to the day and hour
day=$(date +%a)
hour=$(date +%H)


# Then goes to the file corresponding to the day and gets the line.
# This is +1 to account for 00 and 23, as it starts from line 1-24. 
# The file path will have to change to the actual path
needed=$(cat /home/ubuntu/scripts/days/$day.txt | awk '{if(NR=='$hour'+1) print $0}')

# Prints the current needs to the config file
echo "$(awk 'NR==2 {$0="neededServers: '$needed'"} 1' conf.txt)" > conf.txt
