#!/bin/bash
source /home/ubuntu/.openstack_auth
# This script cleans out the certificates from the webservers which have been removed
# When we scale downwards. It does so by taking two variables, which is the current
# servers and the needed. It then runs through a for loop cleaning the certificates of the
# now removed servers.
current=$1
needed=$2

while [ $needed -lt $current ]
do
  current="$(($current-1))"
  puppetserver ca clean --certname www$current.node.consul
done

