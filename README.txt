This script folder contains a working proof of concept for predictive scaling of this Openstack Project.

It's seperated into a configuration file: conf.txt
 - This file is used to keep track of the current webservers running, how many are needed, and the name of the stack.
 - This configuration file is from where the updating script gets information to scale upwards or downwards.

Then there is the days folder
 - This folder contains information about the amount of needed servers for each hour, for every day.
 - Its seperated into seven data files named: Mon.txt, Tue.txt, Wed.txt.... and so on.
 - These have a 24 lines each containing a number. The lines are the hours of the day, 
    while the number in the lines contain the amount of servers needed.

rightDay.sh script: Checking for needed servers
 - This script is ran each hour by a cron script, and it essentially checks the day and hour.
    It then enters the right days .txt file and goes to the line equal to the hour+1. (to account for 00 and 23)
 - Then with the information gotten from entering the text file it updates the config file: config.txt with the new needed
    amount of web servers.

different.sh script: The scaling script
 - This script gets the stack name, needed servers and current servers from the conf.txt file.
    It then checks if there is a need to scale the server up or down, if there is no need it exits.
 - If there is a need then it runs an openstack command to scale the web servers up or down to the needed servers,
    and then updates the conf.txt file with the new current servers.
 - We run this script each 10 minutes starting from 1, so 11, 21, 31 and so on. This is to allow the rightDay script to run
    each hour and update the conf.txt file. In addition this creates the opportunity to dynamically scale in the future,
    by having other scripts interact with the conf.txt file.
    
Future Development:
 - We have a prometheus server running, and the different servers are set up to be running as clients for this server
    This enables us to gather information from the different clients, and also monitor their loads. There are two 
    features which we would implement in the future but did not have time to implement before the deadline of the project
     - Add dynamic scaling: In the sense that we monitor the current load of the servers and dynamically allocate more 
        web servers to account for unexpected sudden loads.
     - Update the predictive data: Using prometheus we can monitor the different webservers, and we can gather data from
        haproxy. We would then use this data to improve the days text files with better and more accurate predictive information.
