#!/bin/bash

source /home/ubuntu/.openstack_auth
# Gets the different variables from the config file
name=$(cat conf.txt | awk '{if(NR==1) print $2}')
needed=$(cat conf.txt | awk '{if(NR==2) print $2}')
current=$(cat conf.txt | awk '{if(NR==3) print $2}')


# If/else statement. Checks if there is a need to scale the server or not.
# If the needed/current is equal then there is no need to scale the server
# and it exits, if they are different then it runs the script/command to change
# the current running servers.

if [ $current -eq $needed ]
then
  exit 1
elif [ $needed -lt $current ]
then
  openstack stack update --existing $name --parameter indexed_group_count=$needed
  echo "$(awk 'NR==3 {$0="currentServers: '$needed'"} 1' conf.txt)" > conf.txt
  ./scaling.sh $current $needed
else
  openstack stack update --existing $name --parameter indexed_group_count=$needed
  echo "$(awk 'NR==3 {$0="currentServers: '$needed'"} 1' conf.txt)" > conf.txt
fi
